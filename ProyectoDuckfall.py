"""

           [DuckFall]
Ingeniería Civil en Bioinformática
  Gerardo Muñoz & Benjamín Rojas

"""

import time
import os
import random


def mapita (map, posX, posY):
    for i in range (0,20):
        linea = ""

        for j in range (0,20):
            casilla = map [i][j]

            #Ficha jugador.
            if (posX == j and posY == i):
                linea += "🦆"

            else:
                #Murallas.
                if casilla == str (0):
                    linea += "⬜"

                #Nada.
                elif casilla == str (1):
                    linea += "  "

                #Inicio.
                elif casilla == str (3):
                    linea += "🏠"

                #Meta (rio).
                elif casilla == str (4):
                    linea += "🌊"

                #Camino.
                elif casilla == str (5):
                    linea += "⬛"

        print (linea)


os.system('clear')

#ANIMACIÓN "ARCADE".
for R in range (0,5):
    print ("   +-------------------------------+")
    print ("   |=====Bienvenido a DuckFall=====|")
    print ("   |==ING. CIVIL EN BIOINFORMÁTICA=|")
    print ("   +-------------------------------+")
    print (" ")
    time.sleep(0.1)
    os.system('clear')
    time.sleep(0.1)

print ("   +-------------------------------+")
print ("   |=====Bienvenido a DuckFall=====|")
print ("   |==ING. CIVIL EN BIOINFORMÁTICA=|")
print ("   +-------------------------------+")
print (" ")
time.sleep(1)

#ANIMACIÓN "ARCADE".
for t in range (0,2):
    print ("   [=========INSTRUCCIONES========]")
    print (" ")
    time.sleep(0.1)

print ("   [=========INSTRUCCIONES========]")
print (" ")
time.sleep(1)

#INSTRUCCIONES Y DESCRIPCIÓN DEL PERSONAJE.
print ("✅ Eres un pato y tienes que salir del laberinto.")
time.sleep (1)
print ("✅ Patito está muy asustado, sólo quiere darse un chapuzón en el río que está ¡al otro lado del laberinto!")
time.sleep (1)
print ("✅ ¿Podrás ayudar a patito a salir del oscuro laberinto?")
time.sleep (1)
print ("✅ ¿Qué por qué eres un pato? Eso no me lo preguntes ;)")
time.sleep (1)
print ("✅ Yo solo soy una computadora 😎")
time.sleep (1)
print (" ")
print ("LAS TECLAS PARA JUGAR SON:")
print ("\n")
time.sleep (1)

#TECLAS DE MOVIMIENTO DENTRO DEL JUEGO.
print ("   +--------------------------------+")
print ("   |             [ W ]              |")
print ("   |       [ A ] [ S ] [ D ]        |")
print ("   +--------------------------------+")
time.sleep (1)

print (" ")
print ("[W] = ARRIBA")
time.sleep (1)
print ("[A] = IZQUIERDA")
time.sleep (1)
print ("[S] = ABAJO")
time.sleep (1)
print ("[D] = DERECHA")
time.sleep (1)
print (" ")


#LABERINTO.
map = ["10000000000000000000",
       "35555555555500005550",
       "10500500000500005000",
       "10555555555555555000",
       "10500050500500000000",
       "10500055500555505500",
       "10000050000500000500",
       "10500055005555555500",
       "10550005500000000500",
       "10050005005500500500",
       "10055005000500555500",
       "10050005000555500500",
       "10055555550000005550",
       "10050005000000000500",
       "10055555000055555500",
       "10050005000050000550",
       "10050005555555550000",
       "10055055000000055550",
       "10000000000000000050",
       "11111111111111111141"]

juego = 0
numerodejugadas = 0
juego_opcion = ""
posX = 1
posY = 1

os.system('clear')


#ANIMACIÓN "ARCADE".
for f in range (0,10):
    print ("\t      ¿¿PREPARADO??")
    time.sleep (0.2)
    print ("\n")

time.sleep (1)
os.system('clear')


print ("\t➡ ESTE ES EL LABERINTO ⬅")
print (" ")

time.sleep (1)

mapita (map, posX, posY)

while (juego == 0):

    print (" ")
    print ("**RECUERDA QUE LAS TECLAS DE MOVIMIENTO SON: [W, A, S, D] ;)**")
    print (" ")
    opcion = input ("Ingrese la dirección en la que se desea mover: ")

    os.system('clear')
    print ("\t      ➡ TABLERO ⬅")
    print (" ")

    numero_aleatorio=random.randint (0,3)

    if (opcion == 'w' or opcion == 'W'):

        if (map[posY-1][posX] != '0'):
            posY = posY -1

        else:
            print (" ")
            print ("MOVIMIENTO NO VÁLIDO. INTENTE NUEVMANTE.")
            if (numero_aleatorio == 0):
                print ("VAMOS PATITO, TU PUEDES")
                print (" ")

            elif (numero_aleatorio == 1):
                print ("ESCUCHA TU CORAZÓN, TU PUEDES")
                print (" ")

            else:
                print ("¡NO TE RINDAS PATITO!")
                print (" ")


    elif (opcion == 's' or opcion == 'S'):

        if (map[posY+1][posX] != '0'):
            posY = posY +1

        else:
            print (" ")
            print ("MOVIMIENTO NO VÁLIDO. INTENTE NUEVMANTE.")
            if (numero_aleatorio == 0):
                print ("VAMOS PATITO, TU PUEDES")
                print (" ")

            elif (numero_aleatorio == 1):
                print ("ESCUCHA TU CORAZÓN, TU PUEDES")
                print (" ")

            else:
                print ("¡NO TE RINDAS PATITO!")
                print (" ")

    elif (opcion == 'a' or opcion == 'A'):

        if (map[posY][posX-1] != '0' and map[posY][posX-1] != '3'):
            posX = posX -1

        else:
            print (" ")
            print ("MOVIMIENTO NO VÁLIDO. INTENTE NUEVMANTE.")
            if (numero_aleatorio == 0):
                print ("VAMOS PATITO, TU PUEDES")
                print (" ")

            elif (numero_aleatorio == 1):
                print ("ESCUCHA TU CORAZÓN, TU PUEDES")
                print (" ")

            else:
                print ("¡NO TE RINDAS PATITO!")
                print (" ")

    elif (opcion == 'd' or opcion == 'D'):

        if (map[posY][posX+1] != '0'):
            posX = posX +1

        else:
            print (" ")
            print ("MOVIMIENTO NO VÁLIDO. INTENTE NUEVMANTE.")
            if (numero_aleatorio == 0):
                print ("VAMOS PATITO, TU PUEDES")
                print (" ")

            elif (numero_aleatorio == 1):
                print ("ESCUCHA TU CORAZÓN, TU PUEDES")
                print (" ")

            else:
                print ("¡NO TE RINDAS PATITO!")
                print (" ")

    mapita (map, posX, posY)
    numerodejugadas = numerodejugadas +1

    print ("")

    #VICTORIA.
    if (map [posY][posX] == '4'):

        print ("\n")
        print ("     +--------------------------------------------------------------------+")
        print ("     |🔥💯🔥💯🔥💯🔥💯¡¡HAS LOGRADO ESCAPAR DEL LABERINTO!!💯🔥💯🔥💯🔥💯 |")
        print ("     |🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥¡FELICIDADES!🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 |")
        print ("     +--------------------------------------------------------------------+")
        print ("     |                      LOGRASTE SALIR EN", numerodejugadas, "JUGADAS                  |")
        print ("     +--------------------------------------------------------------------+")

        print ("\n")
        juego_opcion = input("¿DESEA JUGAR NUEVAMENTE? (s/n): ")

        if (juego_opcion == 's' or juego_opcion == 'S'):
            posX = 1
            posY = 1
            numerodejugadas = 0
            os.system('clear')
            mapita (map, posX, posY)

        elif (juego_opcion == 'n' or juego_opcion == 'N'):
            print ("\n")
            print ("💢💢💢💢💢💢💢💢💢💢¡FIN DEL JUEGO! ¡GRACIAS POR JUGAR!💢💢💢💢💢💢💢💢💢💢")
            print ("\n")
            juego = 1

